package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.project.enforcement.implementation.core.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ImplActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public ImplActivity findById(String id) {
        return mongoTemplate.findById(id, ImplActivity.class);
    }

    public void save(ImplActivity implActivity) {
        mongoTemplate.save(implActivity);
    }

    public void update(ImplActivity implActivity) {
        mongoTemplate.save(implActivity);
    }

    public void delete(String id) throws NotFoundException {
        ImplActivity implActivity = findById(id);
        if (implActivity == null) {
            throw new NotFoundException();
        } else {
            mongoTemplate.remove(implActivity);
        }
    }
}
