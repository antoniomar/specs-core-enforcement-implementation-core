package eu.specs.project.enforcement.implementation.core.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.ChefService;
import org.jclouds.chef.domain.DatabagItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ImplPlanService {

    @Autowired
    private ChefService chefService;

    @Autowired
    private ObjectMapper objectMapper;

    public void storeImplPlan(ImplementationPlan implPlan) throws JsonProcessingException {
        String implPlanJson = objectMapper.writeValueAsString(implPlan);
        chefService.uploadDatabagItem("implementation_plan", implPlan.getId(), implPlanJson);
    }

    public ImplementationPlan retrieveImplPlan(String implPlanId) throws IOException {
        DatabagItem planDatabag = chefService.getDatabagItem("implementation_plan", implPlanId);
        return objectMapper.readValue(planDatabag.toString(), ImplementationPlan.class);
    }
}
