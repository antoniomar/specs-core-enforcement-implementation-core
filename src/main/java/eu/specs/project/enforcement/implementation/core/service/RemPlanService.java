package eu.specs.project.enforcement.implementation.core.service;

import eu.specs.project.enforcement.implementation.core.repository.RemPlanRepository;
import eu.specs.datamodel.enforcement.RemPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RemPlanService {

    @Autowired
    private RemPlanRepository remPlanRepository;

    public RemPlan implementRemPlan(RemPlan remPlan) {
        remPlan.setId(UUID.randomUUID().toString());
        remPlanRepository.save(remPlan);

        // TODO: implement remediation plan

        return remPlan;
    }

    public RemPlan getRemediationPlan(String remPlanId) {
        return remPlanRepository.findById(remPlanId);
    }
}
