package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.project.enforcement.implementation.core.exception.NotFoundException;
import eu.specs.datamodel.enforcement.RemPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RemPlanRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public RemPlan findById(String remPlanId) {
        return mongoTemplate.findById(remPlanId, RemPlan.class);
    }

    public void save(RemPlan remPlan) {
        mongoTemplate.save(remPlan);
    }

    public void update(RemPlan remPlan) {
        mongoTemplate.save(remPlan);
    }

    public void delete(String remPlanId) throws NotFoundException {
        RemPlan remPlan = findById(remPlanId);
        if (remPlan == null) {
            throw new NotFoundException();
        } else {
            mongoTemplate.remove(remPlan);
        }
    }
}
