package eu.specs.project.enforcement.implementation.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.ChefService;
import eu.specs.project.enforcement.broker.CloudService;
import eu.specs.project.enforcement.broker.CloudServiceEucalyptus;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.project.enforcement.broker.CloudServiceEucalyptus.executeIstructionsOnNode;
import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.project.enforcement.contextlistener.ContextListener;
import eu.specs.project.enforcement.implementation.core.exception.NotFoundException;
import eu.specs.project.enforcement.implementation.core.repository.ImplActivityRepository;

import org.apache.http.client.ClientProtocolException;
import org.jclouds.chef.domain.DatabagItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class ImplActivityService {

	@Autowired
	private ImplActivityRepository implActivityRepository;

	@Autowired
	private ChefService chefService;

	@Autowired
	private ImplPlanService implPlanService;

	public ImplActivity implementPlan(ImplActivity implActivity) throws IOException {
		implActivity.setId(UUID.randomUUID().toString());
		implActivity.setCreationTime(new Date());
		implActivity.setState(ImplActivity.Status.CREATED);
		implActivityRepository.save(implActivity);
		//TODO IMPLEMENT

		//      // retrieve implementation plan from the Chef service
		ImplementationPlan plan = implPlanService.retrieveImplPlan(implActivity.getImplPlanId());
		acquireEucalyptusVMs(plan);


		//
		////        BrokerTemplateBean bean = new BrokerTemplateBean();
		////
		////        bean.appliance = plan.getIaas().getZone() + "/" + plan.getIaas().getAppliance();
		////        bean.hw = plan.getIaas().getHardware();
		////        bean.provider = plan.getIaas().getProvider();
		////        bean.zone = plan.getIaas().getZone();
		//        
		//        BrokerTemplateBean bean = cc.getBrokerTemplateBean();
		//
		//        // TODO fill the constructor with the provider credential
		////        ProviderCredential provCred = new ProviderCredential("", "");
		//        ProviderCredential provCred = cc.getProviderCredentials();
		//        ProviderCredentialsManager.add("provider", provCred);
		//        CloudService cloudservice = new CloudService(bean.provider, "root", provCred);
		//
		//        java.util.Date date = new java.util.Date();
		//        System.out.println(new Timestamp(date.getTime()) + " | VMs creating");
		//
		//        InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, bean.zone, bean.hw);
		//
		//        String group = "sla-" + implActivity.getSlaId();
		//
		//        ContextListener cc = new ContextListener();
		//        NodeCredential myCred = cc.brokerCredentials();
		////		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		////		System.out.println("public key: "+myCred.getPublickey());
		//
		//        if (myCred == null) {
		//
		//            return null;
		//        }
		//
		//        // the first parameter is the password used to access via ssh on each node
		//        NodeCredential cred = new NodeCredential("specs.123456",
		//                myCred.getPublickey(),
		//                myCred.getPrivatekey());
		//        NodeCredentialsManager.add("def", cred);
		//
		//        int vmsNumber = plan.getPools().get(0).getVms().size();
		//
		//        try {
		//            NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, vmsNumber, descr,
		//                    NodeCredentialsManager.getCredentials("def"), 22, 80, 11211, 9390, 1514);
		//
		//            java.util.Date myDate = new java.util.Date();
		//            System.out.println(new Timestamp(myDate.getTime()) + " | VMs created");
		//
		//            //-------Update Implementation-Info with nodes info----------------
		//            //			info.status=Status.VMs_Acquired;
		//            List<ClusterNode> nodes = nodesInfo.getNodes();
		//
		//            for (ClusterNode node : nodes) {
		//
		//                String s = node.getPrivateIP();
		//                System.out.println("private Ip: " + s);
		//                //				info.private_IP_Address.add(s);
		//                //				info.public_IP_Address.add(node.getPublicIP());
		//
		//            }
		//
		//            //-----------Preparing VMs : install curl--------------------
		//            java.util.Date mydatenew = new java.util.Date();
		//            System.out.println(new Timestamp(mydatenew.getTime()) + " | VMs preparing");
		//
		//            Thread[] threads = new Thread[nodes.size()];
		//            for (int i = 0; i < threads.length; i++) {
		//                //TODO: prepareEnvironmentScript???
		//                //threads[i] = cloudservice.new ExecuteIstructionsOnNode("root", nodes.get(i), prepareEnvironmentScript(""), cred.getPrivatekey(), false, cloudservice);
		//                threads[i].start();
		//            }
		//
		//            for (int i = 0; i < threads.length; i++) {
		//                try {
		//                    threads[i].join();
		//                } catch (InterruptedException ignore) {
		//                }
		//            }
		//
		//            java.util.Date date1 = new java.util.Date();
		//            System.out.println(new Timestamp(date1.getTime()) + " | VMs prepared");
		//
		//
		//            //			info.status=Status.VMs_Prepared;
		//
		//            //attributo del nodo che viene letto dalla ricetta
		//            String attribute = "{\"implementation_plan_id\":\"" + plan.getId() + "\"}";
		//
		//
		//            java.util.Date date4 = new java.util.Date();
		//            System.out.println(new Timestamp(date4.getTime()) + " | chef node bootstrapping");
		//
		//
		//            chefService.bootstrapChef(group, nodesInfo, cloudservice, attribute);
		//
		//            java.util.Date date5 = new java.util.Date();
		//            System.out.println(new Timestamp(date5.getTime()) + " | chef node bootstrapped");
		//
		//            //			info.status=Status.Chef_Node_Bootstrapped;
		//
		////			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());
		//
		//            //---------------------Executing recipes on nodes ------------------------------------------
		//            for (int i = 0; i < nodes.size(); i++) {
		//
		//                List<List<String>> recipes = new ArrayList<List<String>>();
		//                List<String> temp = new ArrayList<String>();
		//                temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
		//                        + "::" + plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
		//                recipes.add(temp);
		//
		//                java.util.Date date6 = new java.util.Date();
		//                System.out.println(new Timestamp(date6.getTime()) + " | executing recipes: " + Arrays.toString(recipes.get(0).toArray()) + " on node " + group + "-" + nodes.get(i).getPrivateIP() + " publicIP:" + nodes.get(i).getPublicIP());
		//
		//                threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group, myCred.getPrivatekey(), cloudservice);
		//                threads[i].start();
		//            }
		//
		//            for (int i = 0; i < nodes.size(); i++) {
		//                try {
		//                    threads[i].join();
		//                } catch (InterruptedException ignore) {
		//                }
		//            }
		//
		//            if (true) {
		//                java.util.Date date7 = new java.util.Date();
		//                System.out.println(new Timestamp(date7.getTime()) + " | recipes completed");
		//            }
		//
		////			info.status=Status.Recipes_completed;
		//        } catch (Exception e) {
		//            e.printStackTrace();
		//        }

		return implActivity;
	}
	
	private String[] prepareEnvironmentScript(String appliance) {
		String[] script= {
				//				"zypper -n --gpg-auto-import-keys in curl" ,
				//				"curl -O http://curl.haxx.se/ca/cacert.pem",
				//				"mv cacert.pem /etc/ca-certificates/",
				//				"touch .bashrc",
				//				"echo 'export CURL_CA_BUNDLE=/etc/ca-certificates/cacert.pem' > .bashrc",
				"/opt/mos-chef-client/bin/check-chef-server.sh"
		};
		return script;
	}

	private void acquireEucalyptusVMs(ImplementationPlan plan){
		//		// inserisco i valori che descrivono il provider e le sue credenziali
		//		BrokerTemplateBean bean = new BrokerTemplateBean();
		//		bean.appliance="eucalyptus/emi-65c3e382";
		//		bean.hw="c1.medium";
		//		//il provider di eucalyptus e' ec2, mentre quelo di amazon e' aws-ec2
		//		bean.provider="ec2";
		//		bean.zone="ieat01";

		//		bean.appliance=plan.getIaas().getZone()+"/"+plan.getIaas().getAppliance();
		//		bean.hw=plan.getIaas().getHardware();
		//		bean.provider=plan.getIaas().getProvider();
		//		bean.zone=plan.getIaas().getZone();

		//AMAZON
		//		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		//EUCALYPTUS
		//		ProviderCredential provCred = new ProviderCredential("AKIE5G1IFU0ZOWY6VQOQ","Hx8aDZ41f4WV55xmpGiHXLyzsWvDBy9UCvk2n4Ri");

		ContextListener cc = new ContextListener();
		BrokerTemplateBean bean = cc.getDefaultBrokerTemplateBean();
		//		BrokerTemplateBean bean = new BrokerTemplateBean();
		//		bean.setAppliance(plan.getIaas().getZone()+"/"+plan.getIaas().getAppliance());
		//		bean.setHw(plan.getIaas().getHardware());
		//		bean.setProvider(plan.getIaas().getProvider());
		//		bean.setZone(plan.getIaas().getZone());


		ProviderCredential provCred = cc.getDefaultProviderCredentials();
		ProviderCredentialsManager.add("provider", provCred);
		CloudServiceEucalyptus cloudservice = new CloudServiceEucalyptus(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();	
		System.out.println(new Timestamp(date.getTime())+" | VMs creating");	


		//		InstanceDescriptor descr = new InstanceDescriptor(bean.getAppliance(), bean.getZone(), bean.getHw());

		String group = "sla-"+plan.getId();


		NodeCredential myCred = cc.brokerCredentials();
		//		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		//		System.out.println("public key: "+myCred.getPublickey());
		//		System.out.println("private key: "+myCred.getPrivatekey());

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		int vmsNumber = plan.getPools().get(0).getVms().size();


		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, vmsNumber , bean,
					NodeCredentialsManager.getCredentials("def"), 22, 80, 8080, 11211, 9390, 1514);

			//			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , descr,
			//					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			System.out.println(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				System.out.println("ip privato acquisito: "+s);
				System.out.println("ip pubblico acquisito: "+node.getPublicIP());
				System.out.println("node id: "+node.getId());
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : install curl--------------------
			java.util.Date mydatenew= new java.util.Date();	
			System.out.println(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = cloudservice.new executeIstructionsOnNode("root",nodes.get(i),prepareEnvironmentScript(""), cred.getPrivatekey(), true, cloudservice);
				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}


			java.util.Date date1= new java.util.Date();	
			System.out.println(new Timestamp(date1.getTime())+" | VMs prepared");


			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++ ){

				List<List<String>> recipes=new ArrayList<List<String>> ();
				List <String> temp = new ArrayList<String>();
				temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
				//				temp.add("applications::metric-catalogue-app");
				//				temp.add("specs-enforcement-webpool::nginx");
				recipes.add(temp);

				final List<List<String>> myrecipes = recipes;
				java.util.Date date6= new java.util.Date();	
				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				
				final int current = i;
				final List<ClusterNode> mynodes=nodes;
				
				threads[i]= new Thread() {

					@Override
					public void run() {
						super.run();
						String rec = "";
						for(int i=0;i<myrecipes.get(0).size();i++){
							rec=myrecipes.get(0).get(i);
						}
						final String recipes = rec;
						
						String path = ContextListener.CHEF_SERVER_ENDPOINT.replace("/organizations/", "").replace("https:", "http:")+
								":81/mos/cgi/mos-chef-server-core/chef-server-apply-recipe.cgi?token="+
								CloudServiceEucalyptus.EUCALYPTUS_CHEF_SERVER_TOKEN+
								"&node="+mynodes.get(current).getId().replace("eucalyptus/", "")+"&recipe="+recipes;

						System.out.println("path ricette chef e': "+path);
						String res;
						try {
							res = ContextListener.doGet(path);
							//						String res = ContextListener.doGetUncheckSSL(path);
							JsonParser parser = new JsonParser();
							JsonObject o = (JsonObject)parser.parse(res);

							//							if(o.get("message").equals("ok"))
							//								System.out.println("recipe executed properly");
							//							else{
							//								System.out.println("ERROR in recipe execution: "+res);
							long d = 5000;
							for(int i=0;i<10;i++){
								try {
									if(i!=0){
										System.out.println("prima di sleep: "+i);
										sleep(d);
										System.out.println("dopo sleep: "+i);
									}

									String res2 = ContextListener.doGet(path);
									System.out.println("res della get: "+res2);
									JsonObject obj = (JsonObject)parser.parse(res2);
									System.out.println("obj della get: "+obj.toString());
									System.out.println("obj.get message: "+obj.get("message"));
									if(obj.get("message").toString().equals("ok")
											|| obj.get("message").toString().equals("\"ok\"")){
										System.out.println("recipe executed properly");
										break;
									}
								} catch (Exception e1) {
									System.out.println("si è verificato un catch nel catch di ExecuteIstructionsOnNode: "+i);
									e1.printStackTrace();
								}
							}
							//							}
						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}


				};

				//				threads[i]= new Thread(new Runnable() {
				//
				//					@Override
				//					public void run() {
				//						String rec = "";
				//						for(int i=0;i<recipes.get(current).size();i++){
				//							rec=recipes.get(current).get(i);
				//						}
				//						final String recipes = rec;
				//						String path = ContextListener.CHEF_SERVER_ENDPOINT.replace("/organizations/", "").replace("https:", "http:")+
				//								":81/mos/cgi/mos-chef-server-core/chef-server-apply-recipe.cgi?token="+
				//								CloudServiceEucalyptus.EUCALYPTUS_CHEF_SERVER_TOKEN+
				//								"&node="+nodes.get(current).getId().replace("eucalyptus/", "")+"&recipe="+recipes;
				//
				//						System.out.println("path ricette chef e': "+path);
				//						String res;
				//						try {
				//							res = ContextListener.doGet(path);
				//							//						String res = ContextListener.doGetUncheckSSL(path);
				//							JsonParser parser = new JsonParser();
				//							JsonObject o = (JsonObject)parser.parse(res);
				//
				//							if(o.get("message").equals("ok"))
				//								System.out.println("recipe executed properly");
				//							else{
				//								System.out.println("ERROR in recipe execution: "+res);
				//								long d = 10000;
				//								for(int i=0;i<10;i++){
				//									try {
				//										System.out.println("prima di sleep: "+i);
				//										sleep(d);
				//										System.out.println("dopo sleep: "+i);
				//
				//										compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
				//										break;
				//									} catch (Exception e1) {
				//										System.out.println("si è verificato un catch nel catch di ExecuteIstructionsOnNode: "+i);
				//										e1.printStackTrace();
				//									}
				//								}
				//							}
				//						} catch (ClientProtocolException e) {
				//							e.printStackTrace();
				//						} catch (IOException e) {
				//							e.printStackTrace();
				//						}
				//					}
				//				});
				//				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group,myCred.getPrivatekey(), cloudservice);
				threads[i].start();

				//				info.recipes.add(InfoRecipes(recipes.get(i)));
			}

			for(int i=0;i<nodes.size();i++ ){
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;



			//
			//
			//			//			info.status=Status.VMs_Prepared;
			//
			//
			//			//attributo del nodo che viene letto dalla ricetta
			//			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		
			//
			/////* the following part is commented since it works only on Amazon
			//			java.util.Date date4= new java.util.Date();	   
			//			System.out.println(new Timestamp(date4.getTime())+" | chef node bootstrapping");
			//
			//
			//			chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);
			//
			//			java.util.Date date5= new java.util.Date();	
			//			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");
			//
			//			//			info.status=Status.Chef_Node_Bootstrapped;
			//
			////			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());
			////*/
			//			//---------------------Executing recipes on nodes ------------------------------------------
			//			for(int i=0;i<nodes.size();i++ ){
			//
			//				List<List<String>> recipes=new ArrayList<List<String>> ();
			//				List <String> temp = new ArrayList<String>();
			////				temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
			////						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
			//				temp.add("applications::web-container-app");
			////				temp.add("specs-enforcement-webpool::nginx");
			//				recipes.add(temp);
			//
			//				java.util.Date date6= new java.util.Date();	
			//				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				
			//
			//				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group,myCred.getPrivatekey(), cloudservice);
			//				threads[i].start();
			//
			////				info.recipes.add(InfoRecipes(recipes.get(i)));
			//			}
			//
			//			for(int i=0;i<nodes.size();i++ ){
			//				try {
			//					threads[i].join();
			//				} catch (InterruptedException ignore) {}
			//			}
			//
			//			if(true){
			//				java.util.Date date7= new java.util.Date();		
			//				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			//			}

			//			info.status=Status.Recipes_completed;

			// */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public ImplActivity getImplActivity(String implActId) {
		return implActivityRepository.findById(implActId);
	}

	public void deleteImplActivity(String implActId) throws NotFoundException {
		implActivityRepository.delete(implActId);
	}
}
