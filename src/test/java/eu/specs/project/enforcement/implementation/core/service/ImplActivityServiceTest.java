package eu.specs.project.enforcement.implementation.core.service;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
@Ignore
public class ImplActivityServiceTest {

    @Autowired
    ImplActivityService implActivityService;

    @Test
    public void testImplementPlan() throws Exception {
        ImplActivity implActivityData = new ImplActivity();
        implActivityData.setImplPlanId("IMPL_PLAN_ID");
        implActivityData.setSlaId("TEST_SLAT");

        ImplActivity implActivity = implActivityService.implementPlan(implActivityData);

        assertNotNull(implActivity.getId());
        assertEquals(implActivity.getSlaId(), implActivityData.getSlaId());
    }
}